module Simple where

import List

score_single :: Int -> [Int] -> Int
score_single number rolls = sum $ filter (== number) rolls

score_pair :: [Int] -> Int
score_pair [_] = 0
score_pair (ls) =
    let (first:second:xs) = reverse $ sort ls
    in
      if first == second then
        first + second
      else
        score_pair (second:xs)

score_two_pairs :: [Int] -> Int
score_two_pairs [] = 0
score_two_pairs [_] = 0
score_two_pairs (ls) =
    let (first:second:xs) = reverse $ sort ls
    in
      if first == second then
        first + second + score_two_pairs xs
      else
        score_two_pairs (second:xs)

