module Main where

import Test.HUnit
import Simple

main = runTestTT testList

testList = TestList [score_single_test,
                     score_pair_test,
                     score_two_pairs_test]

score_single_test = TestList [
       "should return two for two ones" ~:
       2 ~=? score_single 1 [1,1,2,4,4],
       "should return three for three ones" ~:
       3 ~=? score_single 1 [1,1,1,4,4],
       "should return four for two twos" ~:
       4 ~=? score_single 2 [1,2,1,2,4]
    ]

score_pair_test = TestList [
    "should give 4 for pair of 2" ~:
    4 ~=? score_pair [2, 2, 1, 3, 4],
    "should give 6 for pair of 3" ~:
    6 ~=? score_pair [3, 3, 1, 2, 4],
    "should give 4 for unordered pair of 2" ~:
    4 ~=? score_pair [2, 3, 2, 1, 5],
    "should give 0 for no pairs" ~:
    0 ~=? score_pair [1, 2, 3, 4, 5],
    "should score sum for highest matching pair" ~:
    6 ~=? score_pair [2, 2, 3, 3, 1]
    ]

score_two_pairs_test = TestList [
    "should give sum of pairs" ~:
    8 ~=? score_two_pairs [1, 1, 2, 3, 3],
    10 ~=? score_two_pairs [2,2,1,3,3]
    ]
