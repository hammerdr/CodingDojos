import Test.HUnit
import Yahtzee

main = runTestTT testList

testList = TestList [score_singles_test,
                     score_two_pair_test,
                     score_pairs_test,
                     get_pairs_test]

score_singles_test = TestList [
  "scores ones as sum of ones" ~:
  3 ~=? score_singles 1 [1, 1, 1, 2, 3],
  "scores ones as sum of ones" ~:
  4 ~=? score_singles 1 [1, 1, 1, 1, 3],
  "scores twos as sum of twos" ~:
  4 ~=? score_singles 2 [1, 1, 2, 2, 3]
  ]

score_pairs_test = TestList [
  "scores pair of ones" ~:
  2 ~=? score_pairs [1, 1, 2, 3, 4],
  "scores pair of twos" ~:
  4 ~=? score_pairs [2, 2, 1, 3, 4],
  "scores pair of twos in the middle" ~:
  4 ~=? score_pairs [1, 2, 2, 3, 4],
  "score zero when no pair is found" ~:
  0 ~=? score_pairs [1, 2, 3, 4, 5],
  "score pair that's not consecutive" ~:
  4 ~=? score_pairs [1, 2, 3, 2, 4],
  "score highest pair" ~:
  8 ~=? score_pairs [2, 2, 4, 3, 4]
  ]

score_two_pair_test = TestList [
  "scores pair of 1 and pair of 2" ~:
  6 ~=? score_two_pair [1, 1, 2, 2, 3],
  "score pair of 2 and pair of 4" ~:
  12 ~=? score_two_pair [2, 2, 4, 4, 5],
  "score pair of 5 and pair of 6 with pairs at the end" ~:
  22 ~=? score_two_pair [1, 5, 5, 6, 6],
  "score zero if there's no pairs" ~:
  0 ~=? score_two_pair [1, 2, 3, 4, 5],
  "score zero if there is only one pair" ~:
  0 ~=? score_two_pair [1, 1, 2, 3, 4],
  "score two non consecutive pairs" ~:
  18 ~=? score_two_pair [3, 3, 5, 6, 6]
  ]

get_pairs_test = TestList [
  "no pairs should return empty list" ~:
  [] ~=? get_pairs [1, 2, 3, 4, 5],
  "one pair of 1" ~:
  [1] ~=? get_pairs [1, 1, 2, 3, 4],
  "pair of one and pair of two" ~:
  [1, 2] ~=? get_pairs [1, 1, 2, 2, 3],
  "pair of one and pair of two unsorted" ~:
  [1, 2] ~=? get_pairs [1, 2, 1, 2, 3]
  ]
