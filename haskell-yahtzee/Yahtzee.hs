module Yahtzee where

import List

score_singles :: Int -> [Int] -> Int
score_singles value rolls = sum (filter (== value) rolls)

get_pairs :: [Int] -> [Int]
get_pairs [_] = []
get_pairs rolls = let (first:second:rest) = sort rolls
  in if first == second then first:(get_pairs (second:rest)) else get_pairs (second:rest)

score_pairs :: [Int] -> Int
score_pairs rolls | length pairs > 0 = 2 * (head pairs)
                  | otherwise = 0
  where
    pairs = reverse $ get_pairs rolls

score_two_pair :: [Int] -> Int
score_two_pair rolls | length pairs >= 2 = sum $ map (* 2) pairs
                     | otherwise = 0
  where
    pairs = get_pairs rolls
